# Description

This project is a showcase website for the application GambaPet.
In this website you can know what our values are, what our application
can do, what the professionnal can do in this application, a picture of

# How to start the server

## First time ?

If it's your first time on this project, you must download this project with

>``git clone https://gitlab.com/gambapet/landing-page.git``
>then install package with
>``npm install``
>then run the server with
>``npm run start``

The server will start on [your localhost](http://localhost:4200)

## Already have the project ?

If you already have the project, you may need to pull the latest version of the website with :

>``git pull``
>then update the packages with :
>``npm install``
>then run the server with :
>``npm run start``

The server will start on [your localhost](http://localhost:4200)

# Update the website

If you want to update the website by adding or modifing some component, you need to :

>add all your changes with :
>``git add .``
>add a remark to what you have done with :
>``git commit -am 'your remark'``
>and finally, make a push request with :
>``git push``

This will create a push request and, once the administrator are done with it, it will be add to the real version of the website.

# Authors
This project was create by FB and EA for a school project.