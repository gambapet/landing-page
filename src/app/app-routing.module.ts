import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RootContainerComponent } from './modules/root-container/root-container.component';

const routes: Routes = [
  {
    path: '',
    component: RootContainerComponent,
    loadChildren: () => import( './modules/root-container/root-container.module' ).then( ( m ) => m.RootContainerModule ),
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
