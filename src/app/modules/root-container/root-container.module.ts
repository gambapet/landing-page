import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RootContainerRoutingModule } from './root-container-routing.module';
import { HomepageComponent } from './homepage/homepage.component';
import { Routes } from '@angular/router';

const routes: Routes = [{
  path: '',
  component: HomepageComponent,
}]

@NgModule({
  declarations: [
    HomepageComponent,
    HomepageComponent
  ],
  imports: [
    CommonModule,
    RootContainerRoutingModule
  ]
})
export class RootContainerModule { }
